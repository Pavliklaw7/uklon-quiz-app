const getDefaultState = () => {
  return {
    newQuiz: [
      {
        questions: [
          {
            question: 'Обери сім напрямків, якими займається команда tech/uklon',
            title: 'tech/uklon',
            points: 1,
            style: 'grey',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: 'Розвиток Rider та Driver App'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Розробка та підтримка CRM-системи'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Розробка власної криптовалюти'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Удосконалення та підтримка інструментів управління фінансовими потоками'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Розвиток інтелектуальних систем роботи з даними'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Розвиток напрямку «Delivery»'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Розробка власних картографічних продуктів (картографічні інструменти та робота з геоданими)'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Розробка сервісу оренди авто'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Розвиток сервісу спільних поїздок — «Uklon Share»'
              }
            ]
          },
          {
            question: 'Обери нового гравця в команду tech/uklon, одне із завдань якого — розробка та прототипування .NET проєктів',
            title: 'tech/uklon',
            points: 1,
            style: 'grey',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: `Анастасія Освіта: факультет комп'ютерних наук, КНУ імені Тараса Шевченка Скіли та знання: C#, .NET Core; розуміння мікросервісної та event-driven архітектури; покриття функціоналу тестами (unit, component, e2e etc.) Досвід: п’ять років на позиції «Software Engineer», з яких 1 рік — senior`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Андрій Освіта: програма ІТ та бізнес-аналітики, УКУ Скіли та знання: запуск, управління та розвиток нових продуктів; дослідження та аналіз потреб користувачів; аналіз ринку ІТ-рішень; аналіз потреб і очікувань користувачів Досвід: 1 рік на позицій «Бізнес-аналітик» і 1 рік — «Product Owner»`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Данило Освіта: факультет комп'ютерних наук, КНУБА Скіли та знання: робота з інструментами на проєкті: Jira, TestRail, Postman, Fiddler; робота з платіжними системами; проведення навантажувального тестування; робота з CRM-системами; робота з інструментами: Firebug, Developer tools Досвід: 5 років на позиції Manual QA Engineer`
              }
            ]
          },
          {
            question: 'З чого складається архітектура сервісів Uklon, обери три правильні відповіді',
            title: 'Backend',
            points: 1,
            style: 'grey',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: 'Бази даних «RabbitMQ»'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Архітектура з 50+ мікросервісів'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'База мікросервісів зі стеком (.NET 5, python, node.js)'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Монолітний backend сервіс'
              }
            ]
          },
          {
            question: 'Знайди варіант з правдивими даними про використання Uklon у Львові',
            title: 'Rider App',
            points: 1,
            style: 'blue',
            answers: [
              {
                isRight: false,
                isChecked: null,
                answer: `За рік Львів'яни проїхали в Uklon менше кілометрів ніж Одесити; львів’яни частіше користуються послугою «Silence» ніж «Pets»; більшість завантажує Rider App на IOS, а розраховується за поїздку — готівкою`
              },
              {
                isRight: true,
                isChecked: null,
                answer: `За рік Львів'яни проїхали в Uklon більше кілометрів ніж Харків‘яни; львів’яни використовують клас авто «Kids» частіше ніж мешканці інших міст України; більшість завантажує Rider App на Android, а розраховується за поїздку — карткою`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `За рік Львів'яни проїхали в Uklon більше кілометрів ніж Кияни; львів‘яни використовують послугу «Pets» частіше ніж кияни;  більшість завантажує Rider App на Android, а розраховується за поїздку — карткою`
              }
            ]
          },
          {
            question: 'Обери чотири функції, які з’явилися у застосунку на основі проведення cus-dev з користувачами',
            title: 'Rider App',
            points: 1,
            style: 'blue',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: `Можливість редагувати активне замовлення (додавання адреси призначення та додаткових послуг)`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Фіксація ціни на весь час поїздки`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Поїздка з тваринами`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Замовлення авто для іншої людини`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Самостійне керування ціною поїздки`
              },
              {
                isRight: true,
                isChecked: null,
                answer: `Безготівкові чайові для водіїв`
              },
              {
                isRight: true,
                isChecked: null,
                answer: `Можливість додавати адреси, яких немає на карті Uklon`
              },
              {
                isRight: true,
                isChecked: null,
                answer: `Класи авто: «Kids», «Share» та послуга «Доставка»`
              }
            ]
          },
          {
            question: 'Обери три функції, які допомагають водію заробляти більше',
            title: 'Driver App',
            points: 1,
            style: 'white',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: `Ланцюг замовлень — можливість взяти нове замовлення під час завершення поточного `
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Можливість зручної фільтрації замовлень за різними параметрами (ціна, сектор призначення, дистанція тощо)`
              },
              {
                isRight: true,
                isChecked: null,
                answer: `Безготівкові чайові для водіїв`
              },
              {
                isRight: true,
                isChecked: null,
                answer: `Функція автоматичного ввімкнення платного очікування`
              },
              {
                isRight: false,
                isChecked: null,
                answer: `Функція контролю швидкісного режиму`
              }
            ]
          }
        ]
      },
      {
        questions: [
          {
            question: 'Обери шість технологічних рішень, які використовує tech/uklon',
            title: 'tech/uklon',
            points: 1,
            style: 'grey',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: 'Kubernetes'
              },
              {
                isRight: true,
                isChecked: null,
                answer: '.NET Core'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Pascal'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'GitOps (Argo CD)'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'RabbitMQ'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'PHP'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Cloud платформи (GCP, AWS)'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Kafka'
              }
            ]
          },
          {
            question: 'Склади команду tech/uklon із шести спеціалістів, які працюватимуть над вказаною задачею. Задача така: додати новий клас авто — «Uklon Коптер» ',
            title: 'tech/uklon',
            points: 1,
            style: 'grey',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: 'Frontend-розробник'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'HR-спеціаліст'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Backend-розробник'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Бізнес-аналітик'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'СТО'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Тестувальники'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Дизайнер '
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'СЕО'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'DevOps'
              }
            ]
          },
          {
            question: 'Обери дві операції, які проводить застосунок після виклику авто',
            title: 'Rider App',
            points: 1,
            style: 'blue',
            answers: [
              {
                isRight: false,
                isChecked: null,
                answer: 'Визначення місцеперебування пасажира '
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Пошук і призначення водія'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Оцінка поїздки'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Розрахунок оптимального маршруту'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Перевірка наявності коштів на карті і їхнє списання'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Розрахунок ціни'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Переказ коштів водію '
              }
            ]
          },
          {
            question: `Обери два фактори, які впливають на ціну поїздки. 
            Умови замовлення Uklon: вул.Личаківська, 55 — Головний залізничний вокзал; оплата карткою; клас авто: Uklon Kids; з собою 3 валізи; надворі злива`,
            title: 'Rider App',
            points: 1,
            style: 'blue',
            answers: [
              {
                isRight: false,
                isChecked: null,
                answer: 'Настрій водія'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Наявність дитячого автокрісла'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Тариф за міжміське пересування'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Відсутність опадів'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Промокод'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Додатковий час в дорозі через погоду'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Слизькі дороги'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Додатковий багаж'
              }
            ]
          },
          {
            question: 'Обери чотири фішки застосунку, яких немає у конкурентів',
            title: 'Rider App',
            points: 1,
            style: 'blue',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: 'Замовлення авто для іншої людини'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Фіксація ціни на весь час поїздки'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Послуги «Share» та поїздка з тваринами'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Можливість додати декілька адрес до поїздки'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Користувач сам керує ціною поїздки'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Служба підтримки працює в режимі реального часу'
              }
            ]
          },
          {
            question: 'Обери чотири фішки застосунку, яких немає у конкурентів',
            title: 'Driver App',
            points: 1,
            style: 'white',
            answers: [
              {
                isRight: true,
                isChecked: null,
                answer: 'Фільтрація замовлень'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Відображення точки призначення та ціни до прийняття замовлення'
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Функція контролю швидкісного режиму  '
              },
              {
                isRight: true,
                isChecked: null,
                answer: 'Можливість миттєвого виведення коштів з балансу'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Безготівкові чайові'
              },
              {
                isRight: false,
                isChecked: null,
                answer: 'Відображення рейтингу пасажирів'
              }
            ]
          }
        ]
      }
    ],

    userResponseSkelaton: [null, null, null, null, null, null],
    quizIndex: 0,
    test: null
  }
}

const state = getDefaultState()

const getters = {
  newQuiz: ({newQuiz}) => newQuiz,
  responseSkelaton: ({userResponseSkelaton}) => userResponseSkelaton,
  userResponseSkelaton: ({quizIndex, newQuiz}) => Array([...newQuiz[quizIndex].questions].length).fill(null),
  userQuestionResponse: ({userResponseSkelaton}) => (questionIndex) => userResponseSkelaton[questionIndex],
  quizIndex: ({quizIndex}) => quizIndex,
  correctAnswersArr: ({newQuiz, quizIndex}) => newQuiz[quizIndex].questions.map(question => question.answers.map(answer => answer.isRight).filter(isRight => isRight === true))
}

const mutations = {
  updateCheckedStatus (state, {questionIndex, answerIndex, answerIsRight}) {
    state.newQuiz[state.quizIndex].questions[questionIndex].answers[answerIndex].isChecked = !state.newQuiz[state.quizIndex].questions[questionIndex].answers[answerIndex].isChecked

    if (state.userResponseSkelaton[questionIndex][answerIndex] !== null) {
      state.userResponseSkelaton[questionIndex][answerIndex] = null
    } else {
      state.userResponseSkelaton[questionIndex][answerIndex] = answerIsRight
    }
  },
  resetState (state) {
    Object.assign(state, getDefaultState())
  },
  setResponseSkelaton ({newQuiz, quizIndex, userResponseSkelaton}) {
    userResponseSkelaton = Array([...newQuiz[quizIndex].questions].length).fill(null)
  },
  userQuestionAnswers ({userResponseSkelaton}, {questionIndex, userQuestionsAnswersArrLength}) {
    if (userResponseSkelaton[questionIndex] === null) {
      userResponseSkelaton[questionIndex] = Array(userQuestionsAnswersArrLength).fill(null)
    }
  },
  updateQuizIndex (state, {nextQuizIndex}) {
    state.quizIndex = nextQuizIndex
  }
}

const actions = {
  updateCheckedStatus (ctx, payload) {
    ctx.commit('updateCheckedStatus', payload)
  },
  resetState (ctx) {
    ctx.commit('resetState')
  },
  setResponseSkelaton (ctx) {
    ctx.commit('setResponseSkelaton')
  },
  userQuestionAnswers (ctx, payload) {
    ctx.commit('userQuestionAnswers', payload)
  },
  updateQuizIndex (ctx, payload) {
    ctx.commit('updateQuizIndex', payload)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
