import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'intro',
    component: () => import('../views/IntroPage')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutPage')
  },
  {
    path: '/rules',
    name: 'rules',
    component: () => import('../views/RulesPage')
  },
  {
    path: '/quiz',
    name: 'quiz',
    component: () => import('../views/QuizPage')
  },
  {
    path: '/result',
    name: 'result-page',
    component: () => import('../views/ResultPage')
  }
]

const router = new Router({
  routes
})

export default router

// TODO:
// сверстать блок вопроса
// реализовать открытие аопросв по клику на карточку
// перенсти все вопросы  в стор
